package com.utn.tp_2.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.findNavController
import com.utn.tp_2.Entities.usuarios

import com.utn.tp_2.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment4.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment4 : Fragment() {

    lateinit var v: View
    lateinit var txtCuser : TextView
    lateinit var txtemail : TextView
    lateinit var txttel : TextView
    lateinit var btexit : Button

    var user = usuarios()


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

       v = inflater.inflate(R.layout.fragment_4, container, false)

        txtCuser = v.findViewById(R.id.txt_Cuser)
        txtemail = v.findViewById(R.id.txt_email)
        txttel = v.findViewById(R.id.txt_tel)
        btexit =v.findViewById(R.id.bt_exit)

        return v
    }

    override fun onStart() {
        super.onStart()

        val userName = Fragment4Args.fromBundle(arguments!!).userName
        txtCuser.text = ("Bienvenido " + userName)
        txtemail.text = user.getemail(userName)
        txttel.text = user.getTel(userName)


        btexit.setOnClickListener{
            v.findNavController().navigate(Fragment4Directions.actionFragment4ToFragment1())
        }
    }




    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment4.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment4().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
