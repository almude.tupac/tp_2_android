package com.utn.tp_2.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import android.util.Log
import android.content.ContentValues
import android.widget.Toast


import com.utn.tp_2.Entities.AdminSQLiteOpenHelper
import com.utn.tp_2.Entities.usuarios


import com.utn.tp_2.R
//import kotlinx.android.synthetic.main.fragment_1.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"








/**
 * A simple [Fragment] subclass.
 * Use the [Fragment1.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment1 : Fragment() {

    lateinit var v: View
    lateinit var etxtpassword : EditText
    lateinit var etxtusuario : EditText
    lateinit var btnUsuario : Button
    lateinit var btInit: Button

    var user = usuarios()

   // var usuario : Usuarios ("fdfd","","",23)


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        v = inflater.inflate(R.layout.fragment_1, container, false)
        btInit = v.findViewById(R.id.bt_init)
        btnUsuario = v.findViewById(R.id.bt_nUsuario)
        etxtusuario = v.findViewById(R.id.etxt_usuario)
        etxtpassword = v.findViewById(R.id.etxt_password)


        // Inflate the layout for this fragment
        return v
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment1.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment1().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }


    override fun onStart() {
        super.onStart()

        btInit.setOnClickListener {
            if (etxtusuario.length()>0)
            {
                if (user.getUsuario(etxtusuario.text.toString()) )
                {
                    if(etxtpassword.length()>0)
                    {
                        if (user.getPass(etxtusuario.text.toString()) == etxtpassword.text.toString())
                        {
                            v.findNavController().navigate(Fragment1Directions.actionFragment1ToFragment2(etxtusuario.text.toString()))
                        }
                        else
                        {
                            Snackbar.make ( it,"Contraseña incorrecta", Snackbar.LENGTH_SHORT).show()
                        }
                    }
                    else
                    {
                        Snackbar.make ( it,"Ingrese Contraseña", Snackbar.LENGTH_SHORT).show()
                    }
                }
                else
                {

                    Snackbar.make ( it,"El Usuario no existe", Snackbar.LENGTH_SHORT).show()
                }

            }
            else
            {
                  Snackbar.make ( it,"Ingrese un usuario", Snackbar.LENGTH_SHORT).show()
            }
        }
        btnUsuario.setOnClickListener{
           v.findNavController().navigate(Fragment1Directions.actionFragment1ToFragment3())
        }
    }

}




