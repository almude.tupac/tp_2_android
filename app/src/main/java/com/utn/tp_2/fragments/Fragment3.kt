package com.utn.tp_2.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Button
import com.google.android.material.snackbar.Snackbar
import android.util.Log
import androidx.navigation.findNavController

import com.utn.tp_2.Entities.usuarios


import com.utn.tp_2.R





// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment3.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment3 : Fragment() {

    lateinit var v : View
    lateinit var etxtuser : EditText
    lateinit var etxtemail : EditText
    lateinit var  etxttel : EditText
    lateinit var  etxtpass : EditText
    lateinit var  etxtpass1 : EditText
    lateinit var  btcrear : Button

    var user = usuarios()


    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        v = inflater.inflate(R.layout.fragment_3, container, false)
        etxtuser = v.findViewById(R.id.etxt_user)
        etxtemail = v.findViewById(R.id.etxt_email)
        etxttel = v.findViewById(R.id.etxt_tel)
        etxtpass = v.findViewById(R.id.etxt_pass)
        etxtpass1= v.findViewById(R.id.etxt_pass1)
        btcrear = v.findViewById(R.id.bt_crear)


        // Inflate the layout for this fragment
        return v
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment3.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragment3().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onStart() {
        super.onStart()




        btcrear.setOnClickListener{
            if ((etxtuser.length() > 0) && (etxtemail.length() > 0) && (etxttel.length() > 0) && (etxtpass.length() > 0) && (etxtpass1.length() > 0) )
            {
                if(user.usuarioDisponible(etxtuser.text.toString()))
                {
                    if (((etxtemail.text.toString().indexOf("@", 0, true))!=-1) &&
                        ((etxtemail.text.toString().indexOf(".com", 0, true))==(etxtemail.length()-4)) &&
                        ((etxtemail.text.toString().indexOf("@", 0, true))<(etxtemail.length()-6)))
                    {
                        if((etxtpass.length() > 7)){
                            if (etxtpass.text.toString() == etxtpass1.text.toString()) {
                                user.crearUsuario(
                                    etxtuser.text.toString(),
                                    etxtemail.text.toString(),
                                    etxttel.text.toString(),
                                    etxtpass.text.toString()
                                )
                                v.findNavController()
                                    .navigate(Fragment3Directions.actionFragment3ToFragment4(etxtuser.text.toString()))
                            } else {
                               // Log.d(etxtpass.text.toString(), etxtpass1.text.toString())
                                Snackbar.make(it, "Las contraseñas no coinsiden", Snackbar.LENGTH_SHORT).show()
                            }
                        }
                        else
                        {
                            Snackbar.make(it, "La contraseñas debe tener al menos 8 caracteres", Snackbar.LENGTH_SHORT).show()
                        }

                    }
                    else
                    {
                        Snackbar.make(it, "imail Incorrecto", Snackbar.LENGTH_SHORT).show()
                    }
                }
                else
                {
                    Snackbar.make ( it,"Nombre de usuario no disponible", Snackbar.LENGTH_SHORT).show()
                }

            }
            else
            {
                Snackbar.make ( it,"Datos incompletos", Snackbar.LENGTH_SHORT).show()
            }

        }
    }
}







